﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common
{
    public class MaintainabilityIndexOptions
    {
        public float LinesOfCodeWeight { get; set; } = 1f;
        public float ComplexityWeight { get; set; } = 1f;
        public float CohesionWeight { get; set; } = 1f;
        public float CouplingWeight { get; set; } = 1f;
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Parser.Analyzers;
using Parser.Metrics;
using Parser.Parser;
using CodeAnalyzer.Common;
using Common;

/*
 * Processor
 *
 * This class orchestrates the first and second pass parsers. This includes the following tasks:
 * - Insantiate each parser
 * - Iterate over each file to run the parsers
 * - Build data structures to collect data across files
 * - Combine data structures to collect data across both parser
 *   passes and return a neat Element class to the caller
 *
 * It implements only one method, ParseFiles, which accepts a list of files and an optional class
 * to override the Maintainability Index formula coefficents.
 */
namespace CodeAnalyzer.Processor
{
  public class Processor
  {
    public static IEnumerable<Element> ParseFiles(IEnumerable<string> files, MaintainabilityIndexOptions maintainabilityIndexOptions = null)
    {
      var firstPassLocations = new List<Elem>();
      var dependencies = new Dictionary<string, List<string>>();
      var classMembers = new Dictionary<string, List<string>>();
      var functionMembers = new Dictionary<(string Class, string Method), List<string>>();

      // First pass
      foreach (var file in files)
      {
        CSemiExp semi = new CSemiExp();
        semi.displayNewLines = false;
        if (!semi.open(file))
        {
          throw new ArgumentException($"First pass cannot open file {file}");
        }

        var firstPassCodeAnalyzer = new FirstPassCodeAnalyzer(semi);
        RulesBasedParser firstPassParser = firstPassCodeAnalyzer.build();

          while (semi.getSemi())
            firstPassParser.parse(semi);
          semi.close();

        // Build data across files
        var firstRepo = firstPassCodeAnalyzer?.GetRepository();
        firstPassLocations.AddRange(firstRepo.locations);
        foreach (var member in firstRepo.ClassMembers)
        {
          classMembers[member.Key] = member.Value;
        }
      }

      // Second pass
      foreach (var file in files)
      {
        CSemiExp semi = new CSemiExp();
        semi.displayNewLines = false;
        if (!semi.open(file))
        {
          throw new ArgumentException($"Second pass cannot open file {file}");
        }

        var secondPassCodeAnalyzer = new SecondPassCodeAnalyzer(semi, firstPassLocations, classMembers);
        RulesBasedParser secondPassParser = secondPassCodeAnalyzer.build();

        while (semi.getSemi())
          secondPassParser.parse(semi);
        semi.close();

        // Build data across files
        var secondRepo = secondPassCodeAnalyzer?.GetRepository();
        foreach (var dependency in secondRepo.ClassDependencies)
        {
          dependencies[dependency.Key] = dependency.Value;
        }
        foreach (var member in secondRepo.FunctionMembers)
        {
          functionMembers[member.Key] = member.Value;
        }
      }

      // Build data to be returned
      var elements = firstPassLocations
        .Select(e => new Element
        {
          Type = e.type,
          Name = e.name,
          FunctionClass = e.functionClass,
          LinesOfCode = e.endLine - e.beginLine,
          CyclomaticComplexity = e.endScopeCount - e.beginScopeCount + 1
        })
        .ToList();

      var newElements = new List<Element>();
      foreach (var e in elements)
      {
        if (e.Type == "class")
        {
          // Get class members
          if (classMembers.ContainsKey(e.Name))
          {
            e.Members = classMembers[e.Name];
          }

          // Get class dependencies
          if (dependencies.ContainsKey(e.Name))
          {
            foreach (var d in dependencies[e.Name])
            {
              var element = elements.First(el => el.Name == d);
              e.Dependencies.Add(element);
            }
          }
        }
        else if (e.Type == "function" && functionMembers.ContainsKey((e.FunctionClass, e.Name)))
        {
          e.Members = functionMembers[(e.FunctionClass, e.Name)];
        }

        newElements.Add(e);
      }

      // Calculate metrics
      var classes = newElements.Where(e => e.Type == "class");
      foreach (var c in classes)
      {
        c.Cohesion = Cohesion.Calculate(newElements, c);

        MaintainabilityIndex.Options = maintainabilityIndexOptions ?? new MaintainabilityIndexOptions();
        c.MaintainabilityIndex = MaintainabilityIndex.Calculate(newElements, c);
      }

      return newElements;
    }
  }
}

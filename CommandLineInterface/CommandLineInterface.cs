﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using CodeAnalyzer.Common;
using CodeAnalyzer.Processor;
using Common;

/*
 * CommandLineInterface
 *
 * This class implements the CLI front end. It provides the following interface:
 * - DisplayUsage: Displays built-in documentatin to help the user (examples and options)
 * - ProcessArguments: Accepts user input and runs the analyzer.
 * - DisplayResults: Writes results to the console or to a specified output file
 */
namespace Parser.CLI
{
    public static class CommandLineInterface
  {
    public static List<CliArg> Args = new List<CliArg>
    {
      new CliArg
      {
        Name = "help",
        Required = false,
        ExpectValue = false,
        Description = "Display help"
      },
      new CliArg
      {
        Name = "pattern",
        Required = false,
        ExpectValue = true,
        Description = "File pattern with wildcard (*) [default: *.cs]",
        ValueSample = "<pattern>"
      },
      new CliArg
      {
        Name = "output",
        Required = false,
        ExpectValue = true,
        Description = "Output file to write results to",
        ValueSample = "<file>"
      },
      new CliArg
      {
        Name = "dependencies",
        Required = false,
        ExpectValue = false,
        Description = "Show class dependencies"
      },
      new CliArg
      {
        Name = "methods",
        Required = false,
        ExpectValue = false,
        Description = "Show class methods"
      },
      new CliArg
      {
        Name = "variables",
        Required = false,
        ExpectValue = false,
        Description = "Show class variables"
      },
      new CliArg
      {
        Name = "weights",
        Required = false,
        ExpectValue = true,
        Description = "Override weights to calculate MI (loc,cyc,coh,cou) [default: 1]",
        ValueSample = "<f,f,f,f>"
      },
    };

    public static void DisplayUsage(string error = null)
    {
      if (error == null)
      {
        Console.WriteLine("\nCode Maintainability Analyzer");
      }
      else
      {
        Console.WriteLine($"\nError: {error}");
      }

      // Examples
      Console.WriteLine("\nUsage:");
      Console.WriteLine(@"  .\Parser.exe .                      # Parse all files in this directory");
      Console.WriteLine(@"  .\Parser.exe ""sub/directory""        # Parse all files in subdirectory");
      Console.WriteLine(@"  .\Parser.exe . -methods -v -d       # Show methods, variables, and dependencies");
      Console.WriteLine(@"  .\Parser.exe . --o=""out.txt""        # Show methods, variables, and dependencies");
      Console.WriteLine(@"  .\Parser.exe . -p=""P*.cs""           # Only searches for files with ""cs."" extension");
      Console.WriteLine(@"  .\Parser.exe . -w=""0, .3, .3, .3""   # Ignore LoC metric in MI calculation");

      Console.WriteLine("\nOptions:");
      foreach (var arg in Args)
      {
        var shortName = char.ToLower(arg.Name.First());
        var valueString = arg.ExpectValue ? $"={arg.ValueSample}" : "";
        var flags = $"-{shortName} --{arg.Name}{valueString}";
        Console.WriteLine($"  {flags, -25}{arg.Description}");
      }

      Console.Write("\n");
    }

    public static void ProcessArguments(string[] args)
    {
      // Display help
      if (args.Length == 0 || args[0] == "-h" || args[0] == "--help")
      {
        DisplayUsage();
        return;
      }

      var path = Path.GetFullPath(args[0]);

      foreach(var arg in args.Skip(1)) // Skip path
      {
        CliArg cliArg;
        if (arg.StartsWith("--"))
        {
          cliArg = Args.First(a => string.Equals(
            a.Name,
            arg.Substring(2), 
            StringComparison.CurrentCultureIgnoreCase));
        } 
        else if (arg.StartsWith("-"))
        {
          cliArg = Args.First(a => char.ToUpper(a.Name.First()) == char.ToUpper(arg.SkipWhile(c => c == '-').First()));
        }
        else
        {
          DisplayUsage($"Invalid argument {arg}");
          return;
        }

        if (cliArg.ExpectValue)
        {
          cliArg.Value = arg.Split('=').Last();
        }
        else
        {
          cliArg.Value = true;
        }
      }

      // Set output options
      var outputOptions = new OutputOptions
      {
        ShowMethods = Args.First(a => a.Name == "methods").Value as bool? ?? false,
        ShowVariables = Args.First(a => a.Name == "variables").Value as bool? ?? false,
        ShowDependencies = Args.First(a => a.Name == "dependencies").Value as bool? ?? false,
      };

      // Override MI weights if specified
      var weights = Args
        .First(a => a.Name == "weights")
        .Value as string;
      MaintainabilityIndexOptions miOptions = null;
      if (weights != null)
      {
        try
        {
          var weightNumbers = weights
            .Split(',')
            .Select(float.Parse)
            .ToList();

          miOptions = new MaintainabilityIndexOptions
          {
            LinesOfCodeWeight = weightNumbers[0],
            ComplexityWeight = weightNumbers[1],
            CohesionWeight = weightNumbers[2],
            CouplingWeight = weightNumbers[3],
          };
        }
        catch
        {
          DisplayUsage(@"Weights not in valid format (Example -w=""0.1,0.1,0.4,0.4""");
          return;
        }
      }

      // Redirect Console to file output if specified
      if (Args.First(a => a.Name == "output").Value is string outputFile)
      {
        try
        {
          var ostrm = new FileStream(outputFile, FileMode.Create, FileAccess.Write);
          var writer = new StreamWriter(ostrm);
          Console.SetOut(writer);
        }
        catch (Exception e)
        {
          DisplayUsage($"Cannot open {outputFile} for writing");
          return;
        }
      }

      // Look for all C# source files by default
      var searchPattern = Args.First(a => a.Name == "pattern").Value as string ?? "*.cs";
      var files = Directory.GetFiles(path, searchPattern, SearchOption.AllDirectories).ToList();

      var elements = Processor.ParseFiles(files, miOptions);

      DisplayResults(elements, outputOptions);
    }

    public static void DisplayResults(IEnumerable<Element> elements, OutputOptions options = null)
    {
      options = options ?? new OutputOptions(); // All false if not specified

      var allClasses = elements
        .Where(e => e.Type == "class")
        .OrderByDescending(e => e.MaintainabilityIndex)
        .ToList();

      Console.Write("\n\n");
      Console.WriteLine($"{"Class",-30}{"LoC",-10}{"Complexity",-15}{"Cohesion",-10}{"Coupling",-10}{"MI",-10}");
      Console.WriteLine(new String('=', 80));

      if (allClasses.Count == 0)
      {
        Console.WriteLine("\nNo classes found\n");
        return;
      }

      var averageLoc = allClasses.Average(c => c.LinesOfCode);
      var averageComplexity = allClasses.Average(c => c.CyclomaticComplexity);
      var averageCohesion = allClasses.Average(c => c.Cohesion);
      var averageCoupling = allClasses.Average(c => c.Dependencies.Count);
      var averageMi = allClasses.Average(c => c.MaintainabilityIndex);

      // Display averages
      Console.Write("\n");
      Console.WriteLine($"{"Average",-30}{averageLoc,-10:F0}{averageComplexity,-15:F0}{averageCohesion,-10:F0}{averageCoupling,-10:F0}{averageMi:F0}");
      Console.Write("\n");

      foreach (var c in allClasses)
      {
        Console.WriteLine($"{c.Name,-30}{c.LinesOfCode,-10}{c.CyclomaticComplexity,-15}{c.Cohesion,-10}{c.Dependencies.Count,-10}{c.MaintainabilityIndex:F0}");
        if (options.ShowMethods)
        {
          var methods = elements.Where(e => e.Type == "function" && e.FunctionClass == c.Name);
          foreach (var m in methods)
          {
            Console.WriteLine($"  M:{m.Name,-26}{m.LinesOfCode,-10}{m.CyclomaticComplexity,-15}{"",-10}{"",-10}");
            //Console.WriteLine($"  M:{m.Name}");
          }
        }
        if (options.ShowDependencies)
        {
          if (c.Dependencies.Count > 0)
          {
            foreach (var d in c.Dependencies)
            {
              Console.WriteLine($"  D:{d.Name}");
            }
          }
          else
          {
            Console.WriteLine("  No dependencies");
          }
        }
        if (options.ShowVariables)
        {
          if (c.Members.Count > 0)
          {
            foreach (var m in c.Members)
            {
              var functionUsages = elements
                .Where(e => e.Type == "function" && e.FunctionClass == c.Name && e.Members.Contains(m))
                .Select(e => e.Name);

              var usageString = functionUsages != null ? $" ({String.Join(", ", functionUsages)})" : "";

              Console.WriteLine($"  V:{m}{ usageString }");
            }
          }
        }
      }

      Console.WriteLine("\n");
    }
  }
}

﻿namespace Parser.CLI
{
    public class CliArg
    {
      public string Name { get; set; }
      public bool Required { get; set; }
      public bool ExpectValue { get; set; }
      public string Description { get; set; }
      public object Value { get; set; }
      public string ValueSample { get; set; }
  }
}

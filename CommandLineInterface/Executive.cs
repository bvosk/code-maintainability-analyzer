﻿#define TEST_PARSER

using System;
using Parser.CLI;

/*
 * Executive
 *
 * This is the program entry point and executive. It does nothing but call the CLI interface.
 */
namespace Parser
{
  public class Executive
  {

    //----< Test Stub >--------------------------------------------------

#if(TEST_PARSER)

    static void Main(string[] args)
    {
      try
      {
        CommandLineInterface.ProcessArguments(args);
      }
      catch (Exception e)
      {
        Console.WriteLine($"Something went wrong: {e}\n");
        CommandLineInterface.DisplayUsage();
      }
    }

#endif
  }
}
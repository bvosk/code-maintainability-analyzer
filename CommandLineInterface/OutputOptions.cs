﻿namespace Parser.CLI
{
    public class OutputOptions
    {
      public bool ShowDependencies { get; set; }
      public bool ShowVariables { get; set; }
      public bool ShowMethods { get; set; }
  }
}
